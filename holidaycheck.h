#ifndef HOLIDAYCHECK
#define HOLIDAYCHECK

#include <time.h>
#include <stdio.h>
#include <string.h>
#include "addspecialday.h"

int openHday = 0;

int holidaycheck(char holiday[MAX_HOLIDAYS][40])
{
	time_t rawtime;	
	int nmonth;
	int nday;
	int nweekday;
	struct tm* tm_info;

	time(&rawtime);
	tm_info = localtime(&rawtime);
	nmonth = tm_info->tm_mon+1;
	nday = tm_info->tm_mday;
	nweekday = tm_info->tm_wday;

	switch (nmonth)
	{
	// January 
		case JAN:
			switch (nday)
			{
				case 1:
					addholiday(&openHday,holiday,"New Year's Day");
					break;
			}
			break;

	// February
		case FEB:
			switch (nday)
			{
				case 2:
					addholiday(&openHday,holiday,"Groundhog Day");
					break;
				case 14:
					addholiday(&openHday,holiday,"St. Valentine's Day");
					break;
				case 17:
					addholiday(&openHday,holiday,"Mardi Gras");
					break;
				case 29:
					addholiday(&openHday,holiday,"Leap Day");
					break;
			}
			break;

	// March
		case MAR:
			// Dayligth Saving Time Starts
			// second Sunday of March
			if(nday > 7 && nday < 15 && nweekday == SUN)
				{addholiday(&openHday,holiday,"Daylight Saving Time Starts");}
			switch (nday)
			{
				case 14:
					addholiday(&openHday,holiday,"Pi Day");
					break;
				case 17:
					addholiday(&openHday,holiday,"St. Patrick's Day");
					break;
			}
			break;

	// April
		case APR:
			switch (nday)
			{
				case 1:
					addholiday(&openHday,holiday,"April Fools' Day");
					break;
			}
			break;
	
	// May
		case MAY:
			// Mother's Day
			// The second Sunday of May
			if(nday > 7 && nday < 15 && nweekday == SUN)
				{addholiday(&openHday,holiday,"Mother's Day");}

			switch (nday)
			{
				case 1:
					addholiday(&openHday,holiday,"May Day");
					break;
			}
			break;			

	// June
		case JUN:
			// Father's Day
			// The third Sunday of June
			if(nday > 14 && nday < 22 && nweekday == SUN)
				{addholiday(&openHday,holiday,"Father's Day");}

			break;

	// July
		case JUL:
			switch (nday)
			{
				case 4:
					addholiday(&openHday,holiday,"Fourth of July");
					break;
			}
			break;
	
	// August
		case AUG:
			switch (nday)
			{
			}
			break;	
	
	// September
		case SEP:
			// Labor Day
			// First monday of September
			if(nday < 8 && nweekday == MON)
				{addholiday(&openHday,holiday,"Labor Day");}
			break;

	// October
		case OCT:
			// Columbus Day
			// Second Monday in October
			if(nday > 7 && nday < 15 && nweekday == MON)
				{addholiday(&openHday,holiday,"Columbus Day");}
			
			switch (nday)
			{
				case 31:
					addholiday(&openHday,holiday,"Halloween");
					break;
			}
			break;

	// November
		case NOV:
			// Daylight Saving Time Ends
			// First Sunday of November
			if(nday < 8 && nweekday == SUN)
				{addholiday(&openHday,holiday,"Daylight Saving Time Ends");}
			// Thanksgiving
			// Fourth Thursday of November
			if(nday > 21 && nday < 29 && nweekday == THU)
				{addholiday(&openHday,holiday,"Thanksgiving");}	
			break;

	// December
		case DEC:
			switch (nday)
			{
				case 23:
					addholiday(&openHday,holiday,"Festivus");
					break;
				case 24:
					addholiday(&openHday,holiday,"Christmas Eve");
					break;
				case 25:
					addholiday(&openHday,holiday,"Christmas");
					break;
				case 26:
					addholiday(&openHday,holiday,"Boxing Day");
					break;
			}
			break;
	}
	return openHday;	
}
#endif /* HOLIDAYCHECK */
