#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "addspecialday.h"
#include "holidaycheck.h"
#include "birthdaycheck.h"
#include "moon-phase.h"

int main( int argc, char *argv[] )
{
	time_t rawtime;
	char weekday[10];
	char month[10];
	char nmonth[3];
	char cday[3];
	char year[5];
	char daysuffix[3];
	char hm[7];
	char holiday[MAX_HOLIDAYS][40];
	char birthdays[MAX_HOLIDAYS][40];
	struct tm* tm_info;

	time(&rawtime);
	tm_info = localtime(&rawtime);
	int nHolidays = holidaycheck(holiday);
	int nBirthdays = birthdaycheck(birthdays);

	strftime(weekday, 10, "%A", tm_info);
	strftime(month, 10, "%B", tm_info);
	strftime(nmonth, 3, "%m", tm_info);
	strftime(cday, 3, "%d", tm_info);
	strftime(year, 5, "%Y", tm_info);
	strftime(hm, 7, "%H:%M", tm_info);

	int day = cday[1]-'0';
	day += (cday[0]-'0')*10;
	if (day == 11 || day == 12 || day == 13)
	{
		strcpy(daysuffix, "th");	
	} else if (day % 10 == 1){
		strcpy(daysuffix, "st");	
	} else if (day % 10 == 2){
		strcpy(daysuffix, "nd");
	} else if (day % 10 == 3){
		strcpy(daysuffix, "rd");
	} else {
		strcpy(daysuffix, "th");
	}
		
	FILE *fptr;

	if (fopen(argv[1], "r+") == NULL )
	// Check if a file for today exists.
	// If it doesn't exist, then create one, and give it the proper header.
	{
		fptr = fopen(argv[1], "w");
		fprintf(fptr,"┌%s %s %d%s, %s \n", weekday, month, day, daysuffix, year);
		for(int i = 0; i < nHolidays; i++)
		{
			fprintf(fptr,"├%s\n",holiday[i]); 
		}
		for(int i = 0; i < nBirthdays; i++)
		{
			fprintf(fptr,"├%s Birthday\n",birthdays[i]); 
		}
		if (moon_phase(tm_info->tm_year + 1900, tm_info->tm_mon + 1, tm_info->tm_mday) == 4) {
			fprintf(fptr,"├Full Moon\n"); }
		fprintf(fptr,"╰%s\n\n\n", hm);
	} else { 
	// If the file does exist then add time stamp and line break.
		fptr = fopen(argv[1], "a");
		fprintf(fptr,"\n┌───────────────────────────────────────────────────────────────────────────────\n");
		fprintf(fptr,"╰%s\n\n\n", hm);
	}
	fclose(fptr);
}

