#ifndef BIRTHDAYCHECK
#define BIRTHDAYCHECK
#include <string.h>
#include <time.h>
#include "addspecialday.h"


int openBday = 0;

int birthdaycheck(char birthdays[MAX_HOLIDAYS][40])
{
	// " Birthday" is added on in journal.c
	time_t rawtime;	
	int nmonth;
	int nday;
	int nweekday;
	struct tm* tm_info;

	time(&rawtime);
	tm_info = localtime(&rawtime);
	nmonth = tm_info->tm_mon+1;
	nday = tm_info->tm_mday;
	nweekday = tm_info->tm_wday;

	switch (nmonth)
	{
		case JAN:
			switch (nday)
			{
			// Example birthday
				case 4:
					addholiday(&openBday,birthdays,"Isaac Newton");
					break;
			}
			break;

		case FEB:
			switch (nday)
			{
			}
			break;

		case MAR:
			switch (nday)
			{
			}
			break;

		case APR:
			switch (nday)
			{
			}
			break;

		case MAY:
			switch (nday)
			{
			}
			break;
	
		case JUN:
			switch (nday)
			{
			}
			break;
	
		case JUL:
			switch (nday)
			{
			}
			break;

		case AUG:

			switch (nday)
			{
			}
			break;	

		case SEP:
			switch (nday)
			{
			}
			break;

	
		case OCT:
			switch (nday)
			{
			}
			break;

		case NOV:
			switch (nday)
			{
			}
			break;

		case DEC:
			switch (nday)
			{
			}
			break;
	
	}
	return openBday;	
}
#endif /* BIRTHDAYCHECK */
