#ifndef SPECIALDAY
#define SPECIALDAY
#include <string.h>
#define MAX_HOLIDAYS 10

enum weekday {SUN,MON,TUE,WED,THU,FRI,SAT};
enum months {JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC};

int addholiday(int *openHday, char hday[MAX_HOLIDAYS][40], char * dayname)
{
	strcpy(hday[*openHday], dayname);
	*openHday = *openHday + 1;
}
#endif /* SPECIALDAY */

