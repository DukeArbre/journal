Program is used with 'journal' and takes a file path argument.
journal ~/today.txt
would create today.txt or append a new time stamp if the file already exists.

Below is an example of format of the journal.

┌Tuesday February 23rd, 2016 

├Full Moon

╰10:32


First entry.


┌──────────────────────────────────

╰11:12


Second entry.